#%%
#isinstance()
isinstance(4,int)
type(4) == int

class hema:
    def __init__(self):
        pass

x =  hema()
#%%
isinstance(12,str)
#%%
isinstance([],list)
a=[1, 2, 'y', 4, 5]
#%%
isinstance(a,(int,list))

#%%
class A():
    pass
class B(A):
    pass
obj_1 = B()
obj_2 = A()
isinstance(obj_1,A)
isinstance(obj_2,B)
#%%
#basestring()
s="hello"
isinstance(s,str)
u = u'hello'

isinstance(u,unicode)

isinstance(s,basestring)
#%%
#bin()
bin(10)
bin(15)
bin(120)

number = 5
print('the binary number is 5:',bin(number))

#%%
#bool()
t = []
print(t,'is',bool(t))

t=[0]
print(t,'is',bool(t))

t=0.0
print(t,'is',bool(t))
t=True
print(t,'is',bool(t))

A='Hema'
print(A,'is',bool(A))

#%%
mystr1="easy to learn"
mybytes1=mystr1.encode()
print(mybytes1)
#%%
#callable()

def is_even():
    return x%2==0

callable(is_even)
is_odd = lambda x: x % 2 == 1
callable(is_odd)
callable(list)
callable(list.append)

#%%
# ord
print ord('F')
print ord('a')
print(u'\u2034')

#%%
#chr()
print chr(97)
print chr(70)

#%%
pow(2, 5)
xrange(1, 10)

x = [1, 2, 3, 4]
x.append(5)
x.remove(2)
del x[2]
x.insert(2, 0)
x
x.reverse()
x[start:end:step]
x = {'A': 10, 'p': 200, 'l': 1, 'e': 1}

'aaacccaffhlhjjjkh'
'acafhlhjkh'

a_list=[]
for number in range(1,10):
    if number%2 == 1:
        a_list.append(number)
a_list

#%%
#Map
items = [1,2,3,4,5]
list(map(lambda x : x**2, items))
# %%
a = [1,2,3,4,5]
b = [5,6,7,8,9]
map(lambda x,y:x+y, a,b)
