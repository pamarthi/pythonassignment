lst = [1, 2, [2, 5, 6], 4]
result = []
for i in lst:
    if type(i) == list:
        for j in i:
            result.append(j)
    else:
        result.append(i)
result
#%%
def flatter(lst):
    result = []
    for i in lst:
        if type(i) == list:
            result = result + flatter(i)
        else:
            result.append(i)
    return result

flatter([1, 2, [2, [5, 6]], 4])

#%%
#read file
file=open("in built.py","w")
file.write("Hello World\n")
file.write("This is our new text file")
file.write("and this is another line.")
file.write("Why? Because we can.")
file.close()

file=open("in built.py","r")
print file.read()

file=open("in built.py","r")
print file.read(5)

file=open("in built.py","r")
print file.readline()

file=open("in built.py","r")
print file.readlines()

file=open("in built.py","r")
for line in file:
    print line
#%%
#perfect
n=9
sum1=0
for i in range(1,n):
    if(n%i==0):
        sum1=sum1+i
if(sum1==n):
    print("the number is a perfect number")
else:
    print("the number is not a perfect number!")
#%%
#strings
import string
result=string.ascii_letters
print(result)

c='A'
print("the ascii value of "+c+" is", ord(c))

import string
result=string.ascii_lowercase
print(result)

import string
result=string.ascii_uppercase
print(result)

import string
result=string.digits
print(result)

import string
result=string.hexdigits
print(result)

import string
result=string.letters
print(result)

import string
result=string.lowercase
print(result)

import string
result=string.uppercase
print(result)

import string
result=string.octdigits
print(result)

import string
result=string.punctuation
print(result)

import string
result=string.printable
print(result)
import string
result=string.whitespace
print(result)

#%%
#pascal triangle
a=3
a=[]
for i in range(n):
    a.append([])
    a[i].append(1)
    for j in range(1,i):
        a[i].append(a[i-1][j-1]+a[i-1][j])
    if(n!=0):
        a[i].append(1)
for i in range(n):
    print(" "*(n-i),end=" ",sep= " ")
    for j in range(0,i+1):
         print('{0:6}'.format(a[i][j]),end==" ",sep == " ")
    print()
#%%
n=int(input("Enter number of rows: "))
a=[]
for i in range(n):
    a.append([])
    a[i].append(1)
    for j in range(1,i):
        a[i].append(a[i-1][j-1]+a[i-1][j])
    if(n!=0):
        a[i].append(1)
for i in range(n):
    print(" " * (n-i), 'end' == " ", 'sep' == " ")
    for j in range(0,i+1):
        print('{0:6}'.format(a[i][j]), 'end' =" ", 'sep' =" ")
    print()
#%%
x = format(0.5, 'x')
print(x)

#%%
a=10
b=20
if a>b:
    min1=a
else:
    min1=b
while(1):
    if(min1%a==0 and min1%b==0):
        print("lcm is:",min1)
        break
    min1=min1+1
#%%
a=int(input("Enter the first number:"))
b=int(input("Enter the second number:"))
if a>b:
    x=a
else:
    x=b
while(1):
    if(x%a==0 and x%b==0):
        print("LCM of 5 and 3:", x)
        break
    x=x+1

#%%
def gcd( n1 , n2):
    if n2 % n1 == 0 :
        return n1
    else:
        return gcd( n2%n1 , n1)
g = gcd(30,36)
print("gcd of 10 and 20:",g)
#%%
def gcd(a,b):
    if(b==0):
        return a
    else:
        return gcd(b,a%b)
a=60
b=20
print(gcd(60,48))
#%%
import math
print (math.gcd(60,48))
#%%
def print_factors(x):
    print("the factors of",x,"are:")
    for i in range(1,x+1):
        if x%i==0:
            print(i)
num=120
print_factors(num)
