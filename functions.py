def commentary(color):
    if color == 'red':
        return "It's a tomato."
    elif color == "green":
        return "It's a green pepper."
    elif color == 'bluish':
        return "I don't know what it is, but only bees can see it."
    else:
        return "I've never heard of the color " + color + "."
comment = commentary('blue')
print (comment)
#%%
thing = none
if thing:
    print("it is something")
else:
    print("it is nothing")
#%%
def is_none(thing):
    if thing is None:
        print("It's None")
    elif thing:
        print("It's True")
    else:
        print("It's False")
is_none(None)
is_none(0.0)
is_none(True)
is_none([])
#%%
def menu(wine, entree, desert):
    return{'wine':wine,'entree':entree,'desert':desert}
menu('chicken','cake','milk shake')
menu('mutton','chocos','ice cream')
#%%
menu(entree='chocos',wine='beer',desert='cool drinks')
#%%
def menu(wine,entree,desert='pudding'):
    return{'wine':wine,'entree': entree,'desert':desert}
menu('chick','cake')
#%%
def buggy(arg, result=[]):
result.append(arg)
print(result)
    buggy('a')
#%%
def works(arg):
result = []
result.append(arg)
return result
works('a')
#%%
def print_args(*args):
    print('Positional argument tuple:', args)
print_args()
print_args(3, 2, 1, 'wait!', 'uh...')
#%%
def print_more(req1,req2,*args):
    print('need this one:',req1)
    print('need this one too:',req2)
    print('all the best:',args)
print_more('cap', 'gloves', 'scarf', 'monocle', 'mustache wax')
#%%
def print_kwargs(**kwargs):
    print('Keyword arguments:', kwargs)
    print_kwargs(wine='merlot', entree='mutton', dessert='macaroon')
#%%
def inc(x):
    return x+1
# %%
#map()
map(lambda x:x+1,[1, 2, 3, 4, 5])
# %%
