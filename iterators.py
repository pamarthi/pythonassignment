word = 'cat'
for letter in word:
  print(letter)
# %%
days = ['monday', 'tuesday', 'wednesday']
fruits = ['banana', 'apple', 'orange']
drinks = ['coffee', 'tea', 'milk']
for day, fruit, drink in zip(days,fruits,drinks):
    print(day, ": drink", drink,"- eat",fruit)
#%%
list(zip(fruits,drinks))
#%%
dict(zip(fruits,drinks))
# %%
for x in range(0,3):
    print(x)
#%%
list(range(0,3))
#%%
for x in range(2,-1,-1):
    print(x)
#%%
list(range(0, 11, 2))
#%% Comprehensions
#List Comprehensions
number_list=[]
number_list.append(1)
number_list.append(2)
number_list.append(3)
number_list
# %%
number_list=[]
for number in range(1,6):
    number_list.append(number)
number_list
#%%
number_list = list(range(1,6))
number_list
# %%
number_list = [number for number in range(1,6)]
number_list
#%%
number_list=[number-1 for number in range(1,6)]
number_list
# %%
list = [number for number in range(1,6) if number % 2 == 1]
list
#%%
list = [number for number in range(1,6) if number % 2 != 1]
list
#%%
a_list=[]
for number in range(1,11):
    if number % 2 == 1:
        a_list.append(number)
a_list
#%%
rows=range(1,4)
cols=range(1,3)
for row in rows:
    for col in cols:
        print(row,col)
#%%
rows=range(1,4)
cols=range(1,3)
cells=[(row,col)for row in rows for col in cols]
for cell in cells:
    print(cell)
#%%
def agree():
    return True
if agree():
    print('splendid')
else:
    print('that was unexpected')
#%%
rows = range(1,4)
cols = range(1,3)
cells = ((row, col) for row in rows for col in cols)
cells

def fun(*arg):
    print("arguments", arg)

fun(1, 2, 3, 5, 6)
x = [1, 2, 4, 5]
fun(*x)

def fun(**kvarg):
    print("Key value", kvarg)

fun(**{'fdgd': 1, 'drfter': 3})


def outer(a, b):
    def inner(c, d):
        return a + b + c + d
    return inner

x = outer(1, 2)
x(0, 0)
x = outer
x1 = x(1, 2)
x1(3, 5)

x1 = lambda x: x+1
x1(10)

lst = [1, 12, 15, 8]
for i in range(len(lst)):
    lst[i] = lst[i] + 2

for i in range(len(lst)):
    lst[i] = lst[i] + 2

lst
map(lambda x: x + 2, map(lambda x: x + 2, xrange(1, 6)))

def mymap(fun, lst):
    for i in lst:
        print(fun(i))

mymap(lambda x: x + 2, xrange(1, 6))

filter(lambda x: x % 2 == 0, xrange(1, 10))

for i in range(1, 10):
    if i % 2 == 0:
        print(i)

lst = [1, 2, 3, 6]
it = iter(lst)
next(it)


def add(x, y):
    return x + y

reduce(add, [3, 4, 6])

def mysum(lst):
    result = 0
    for i in lst:
        result += i
    return result


mysum([3, 4, 6])

x = input('give input: ')
type(x)
