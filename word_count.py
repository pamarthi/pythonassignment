def word_count(str):
    counts=dict()
    words=str.split()
    for word in words:
        if word in counts:
            counts[word]+=1
        else:
            counts[word]=1
    return counts
print(word_count('a p p l e'))

#%%
# eval()
x = 1
eval('x + 1')
eval('x')


a=25
eval(sqrt('a'))

#%%
#float()
print(float(10))

print(float(5.45345))

print(float("     -24.45\n"))

print(float("NAN"))
print(float("infinity"))

print(float("INFINITY"))
